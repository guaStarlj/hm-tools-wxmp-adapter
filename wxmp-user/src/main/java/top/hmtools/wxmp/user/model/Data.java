package top.hmtools.wxmp.user.model;

import java.util.List;

/**
 * Auto-generated: 2019-08-14 15:11:25
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Data {

	private List<String> openid;

	public void setOpenid(List<String> openid) {
		this.openid = openid;
	}

	public List<String> getOpenid() {
		return openid;
	}

	@Override
	public String toString() {
		return "Data [openid=" + openid + "]";
	}

	
}