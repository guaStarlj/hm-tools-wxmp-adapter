package top.hmtools.wxmp.message.group.model.event;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class CopyrightCheckResultType {

	@XStreamAlias(value = "Count")
	protected String count;
	
	@XStreamAlias(value = "ResultList")
	protected List<ItemType> resultList;
	
	@XStreamAlias(value = "CheckState")
	protected String checkState;

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}


	public List<ItemType> getResultList() {
		return resultList;
	}

	public void setResultList(List<ItemType> resultList) {
		this.resultList = resultList;
	}

	public String getCheckState() {
		return checkState;
	}

	public void setCheckState(String checkState) {
		this.checkState = checkState;
	}

	@Override
	public String toString() {
		return "CopyrightCheckResultType [count=" + count + ", resultList=" + resultList + ", checkState=" + checkState
				+ "]";
	}

}
