package top.hmtools.wxmp.message.template.model;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class TemplateIdResult extends ErrcodeBean {

	private String template_id;

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	@Override
	public String toString() {
		return "TemplateIdResult [template_id=" + template_id + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}
