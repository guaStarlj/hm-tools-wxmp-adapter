package top.hmtools.httpclient.hm2020test;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Test;

import top.hmtools.wxmp.core.httpclient.HmHttpClientFactoryHandle;
import top.hmtools.wxmp.core.httpclient.impl.ConnectionKeepAliveStrategyDemoImpl;
import top.hmtools.wxmp.core.httpclient.impl.HttpRequestRetryHandlerDemoImpl;
import top.hmtools.wxmp.core.httpclient.responseHandler.ResponseHandlerInputStream;

public class HttpClientFactoryHandleTest {

	private String url = "https://www.baidu.com";

	/**
	 * 基于连接池，同一个httpclient反复请求同一个网址
	 */
	@Test
	public void testPoolingAAA() {
		HttpGet get = new HttpGet(url);
		
		//设置 httphost 可提高速度
		String host = get.getURI().getHost();
		int port = get.getURI().getPort();
		HttpHost httpHost =null;
		if(port>0){
			httpHost = new HttpHost(host, port);
		}else{
			httpHost = new HttpHost(host);
		}
		
		//  设置长连接
		HmHttpClientFactoryHandle.setConnectionKeepAliveStrategy(new ConnectionKeepAliveStrategyDemoImpl());
		
		//	设置重试机制
		HmHttpClientFactoryHandle.setHttpRequestRetryHandler(new HttpRequestRetryHandlerDemoImpl());
		
		// 设置超时
		HmHttpClientFactoryHandle.setRequestConfigBuilder(1000, 1000, 10);
		
		//设置代理
//		HttpClientHandle2020.addProxies(new HttpHost("218.75.158.153",3128));
		
		//获取httpclient
		CloseableHttpClient httpClient = HmHttpClientFactoryHandle.getPoolingHttpClient();//基于连接池、自定义
//		CloseableHttpClient httpClient = HttpClientHandle2020.getSimpleHttpClient();//基于自定义
//		CloseableHttpClient httpClient = HttpClientHandle2020.getSimpleDefaultHttpClient();//基于缺省
		
		//最初的时间戳
		long start = System.currentTimeMillis();
		
		//执行循环请求
		CloseableHttpResponse httpResponse = null;
		for (int ii = 0; ii < 100; ii++) {
			//获取httpclient
//		CloseableHttpClient httpClient = HttpClientHandle2020.getPoolingHttpClient();//基于连接池、自定义
//		CloseableHttpClient httpClient = HttpClientHandle2020.getSimpleHttpClient();//基于自定义
//			CloseableHttpClient httpClient = HttpClientHandle2020.getSimpleDefaultHttpClient();//基于缺省
			
			try {
				//局部开始时间戳
				long localStart = System.currentTimeMillis();
				
				//执行http请求
				InputStream inputStream = httpClient.execute(httpHost,get,new ResponseHandlerInputStream());
//				httpResponse = httpClient.execute(httpHost,get);
//				System.out.println(ii + "、httpClient实例是：" + httpClient + "，目标网站返回http状态为："
//						+ httpResponse.getStatusLine().getStatusCode() + "，历时"
//						+ (System.currentTimeMillis() - localStart) + "毫秒");
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
//				if (httpResponse != null) {
//					try {
//						httpResponse.close();
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//				}
			}
		}
		
		System.out.println("总历时" + (System.currentTimeMillis() - start) + "毫秒");
	}


}
