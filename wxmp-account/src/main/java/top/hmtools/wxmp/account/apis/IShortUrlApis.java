package top.hmtools.wxmp.account.apis;

import top.hmtools.wxmp.account.models.ShorturlParam;
import top.hmtools.wxmp.account.models.ShorturlResult;
import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;

@WxmpMapper
public interface IShortUrlApis {

	/**
	 * 长链接转短链接接口
	 * @param shorturlParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/shorturl")
	public ShorturlResult getShorUrl(ShorturlParam shorturlParam);
}
